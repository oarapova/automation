package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class OnboardingPage extends CommonPage {
    public OnboardingPage(WebDriver driver){super(driver);}

    public void assertIsOnPage(String expectedUrl){
        Assert.assertTrue(getUrl().contains(expectedUrl));

    }
}
