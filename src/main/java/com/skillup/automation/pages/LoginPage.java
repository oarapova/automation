package com.skillup.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class LoginPage extends CommonPage{

    private static final String LOGIN_URL = "https://www.linkedin.com/uas/login";
    private static final String EMAIL_PHONE_CSS_LOCATOR = "#username";
    private static final String SESSION_PASS_CSS_LOCATOR = "input[name='session_password']";
    private static final String SIGN_IN_BUTTON_CSS_LOCATOR = "button.btn__primary--large";
    private static final String EMAIL_PHONE_ERROR_MESSAGE = "#error-for-username";
    private static final String PASSWORD_ERROR_MESSAGE = "#error-for-password";

    public LoginPage(WebDriver driver){
        super(driver);
    }

    public LoginPage open(){
        driver.get(LOGIN_URL);
        return this;
    }

    public LoginPage setEmailPhone(String emailPhone){
        enterText(EMAIL_PHONE_CSS_LOCATOR, emailPhone);
        return this;
    }

    public LoginPage setPassword(String password){
        enterText(SESSION_PASS_CSS_LOCATOR, password);
        return this;
    }

    public LoginPage clickSignInButton(){
        click(SIGN_IN_BUTTON_CSS_LOCATOR);
        return this;

    }

    public LoginPage assertEmailErrorMessage(String expectedErrorMessage){
        WebElement errorMessage = driver.findElement(By.cssSelector(EMAIL_PHONE_ERROR_MESSAGE));

        String actualErrorMessage = errorMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
        return this;

    }

    public void assertPasswordErrorMessage(String expectedPasswordMessage){
        WebElement errorMessage = driver.findElement(By.cssSelector(PASSWORD_ERROR_MESSAGE));

        String actualErrorMessage = errorMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedPasswordMessage);

    }



    public String getEmailErrorMessage(){
        WebElement alertMessage = driver.findElement(By.cssSelector(EMAIL_PHONE_ERROR_MESSAGE));
        return alertMessage.getText();
    }


}
