package com.skillup.automation.pages;

import com.skillup.automation.ElementHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import static com.skillup.automation.StringGeneratorUtils.emailGeneration;


public class SignUpPage extends CommonPage{

    private static final String SIGN_UP_URL = "https://www.linkedin.com/start/join?trk=guest_homepage-basic_nav-header-join";
    private static final String FIRST_NAME_CSS_LOCATOR = "#first-name";
    private static final String LAST_NAME_CSS_LOCATOR = "input#last-name";
    private static final String EMAIL_JOIN_CSS_LOCATOR = "#join-email";
    private static final String AGREE_AND_JOIN_CSS_LOCATOR = "button.btn";
    public static final String JOIN_PASS_CSS_LOCATOR = "#join-password";
    public static final String ERROR_ALERT_MESSAGE = ".uno-alert strong";

    private ElementHelper helper = new ElementHelper();

    public SignUpPage(WebDriver driver){
        super(driver);
    }

    public SignUpPage open(){
        driver.get(SIGN_UP_URL);
        return this;
    }

    public SignUpPage enterFirstName(String firstName){
        enterText(FIRST_NAME_CSS_LOCATOR, firstName);
        return this;

    }

    public SignUpPage enterLastName(String lastName){
        enterText(LAST_NAME_CSS_LOCATOR, lastName);
        return this;

    }

    public SignUpPage enterEmail(){
        enterText(EMAIL_JOIN_CSS_LOCATOR, emailGeneration());
        return this;

    }

    public SignUpPage enterPassword(String password){
        enterText(JOIN_PASS_CSS_LOCATOR, password);
        return this;

    }

    public SignUpPage clickJoinButton(){
        click(AGREE_AND_JOIN_CSS_LOCATOR);
        return this;

    }

    public SignUpPage assertErrorMessage(String expectedErrorMessage){
        WebElement errorMessage = driver.findElement(By.cssSelector(ERROR_ALERT_MESSAGE));

        String actualErrorMessage = errorMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
        return this;

    }

}
