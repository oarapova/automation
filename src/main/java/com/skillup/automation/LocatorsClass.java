package com.skillup.automation;

public class LocatorsClass {
    public static final String GOOGLE_BUTTON = "//div[@class='FPdoLc VlcLAe']//input[@name='btnK']";
    public static final String SEARCH_INPUT = "//*[@title='Пошук']";

    public static final String EMAIL_XPATH_LOCATOR = "//input[@name='emailAddress']";
    public static final String EMAIL_CSS_LOCATOR = "input[name='emailAddress']";
    public static final String JOIN_PASS_XPATH_LOCATOR = "//input[@id='join-password']";
    public static final String JOIN_PASS_CSS_LOCATOR = "#join-password";
    public static final String AGREE_AND_JOIN_XPATH_LOCATOR = "//button[@class='btn join-btn-secondary join-btn']";
    public static final String AGREE_AND_JOIN_CSS_LOCATOR = "button.btn";
    public static final String FB_BUTTON_XPATH_LOCATOR = "//button[@class='fb-btn']";
    public static final String FB_BUTTON_CSS_LOCATOR = "button.fb-btn";
    public static final String SIGN_IN_LINK_XPATH_LOCATOR = "//a[@class='sign-in-link']";
    public static final String SIGN_IN_LINK_CSS_LOCATOR = "a.sign-in-link";

    public static final String EMAIL_PHONE_XPATH_LOCATOR = "//*[@id='username']";
    public static final String EMAIL_PHONE_CSS_LOCATOR = "#username";
    public static final String SESSION_PASS_XPATH_LOCATOR = "//input[@name='session_password']";
    public static final String SESSION_PASS_CSS_LOCATOR = "input[name='session_password']";
    public static final String PASS_VISIBILITY_XPATH_LOCATOR = "//span[@id='password-visibility-toggle']";
    public static final String PASS_VISIBILITY_CSS_LOCATOR = "#password-visibility-toggle";
    public static final String SIGN_IN_BUTTON_XPATH_LOCATOR = "//button[@class='btn__primary--large from__button--floating']";
    public static final String SIGN_IN_BUTTON_CSS_LOCATOR = "button.btn__primary--large";
    public static final String FORGOT_PASS_XPATH_LOCATOR = "//a[@class='btn__tertiary--medium action__btn']";
    public static final String FORGOT_PASS_CSS_LOCATOR = "a.btn__tertiary--medium";
    public static final String JOIN_NOW_LINK_XPATH_LOCATOR = "//a[contains(text(), 'Join now')]";
    public static final String JOIN_NOW_LINK_CSS_LOCATOR = "div.footer-app-content-actions>p>a";


    public static final String FIRST_NAME_CSS_LOCATOR = "#first-name";
    public static final String LAST_NAME_CSS_LOCATOR = "input#last-name";
    public static final String EMAIL_JOIN_CSS_LOCATOR = "#join-email";
    public static final String ERROR_ALERT_MESSAGE = ".uno-alert strong";












}
