package com.skillup.automation;

import com.mifmif.common.regex.Generex;

public class StringGeneratorUtils {
    public static String emailGeneration(){
        String regex = "\\w{10}\\@gmail\\.com";
        String email = new Generex(regex).random();
        return email;
    }

//    public static String generateString(){
//        return "";
//    }
//
//    public static String generateEmail(){
//        return generateString() + "@gmail.com";
//    }
//
//    public static String generateEmail2(){
//        return "autotestEmail" + Math.round(Math.random() * 1000) + "@gmail.com";
//        return String.format("autotestEmail%@gmail.com", Math.round(Math.random() * 1000));
//    }
}
