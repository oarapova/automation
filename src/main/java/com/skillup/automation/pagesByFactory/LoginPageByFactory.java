package com.skillup.automation.pagesByFactory;

import com.skillup.automation.ElementHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static com.skillup.automation.configuration.Urls.LOGIN_URL;

public class LoginPageByFactory {

    @FindBy(css = "#username")
    private WebElement emailInput;
    @FindBy(css = "input[name='session_password']")
    private WebElement passwordInput;
    @FindBy(css = "button.btn__primary--large")
    private WebElement loginButton;
    @FindBy(css = "#error-for-username")
    private WebElement errorFormMessage;

    private WebDriver driver;
    private ElementHelper elementHelper = new ElementHelper();

    public LoginPageByFactory(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public LoginPageByFactory open(){
        driver.get(LOGIN_URL);
        return this;
    }

    public LoginPageByFactory setEmailPhone(String emailPhone){
        elementHelper.enterText(emailInput, emailPhone);
        return this;
    }

    public LoginPageByFactory setPassword(String password){
        elementHelper.enterText(passwordInput, password);
        return this;
    }

    public LoginPageByFactory clickSignInButton(){
        loginButton.click();
        return this;

    }

    public LoginPageByFactory assertEmailErrorMessage(String expectedErrorMessage){

        String actualErrorMessage = errorFormMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Error message doesn't match");

        return this;

    }

    public LoginPageByFactory assertPasswordErrorMessage(String expectedPasswordMessage){
        return this;
    }



}
