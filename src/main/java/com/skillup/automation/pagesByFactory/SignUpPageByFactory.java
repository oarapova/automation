package com.skillup.automation.pagesByFactory;

import com.skillup.automation.ElementHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static com.skillup.automation.StringGeneratorUtils.emailGeneration;
import static com.skillup.automation.configuration.Urls.SIGN_UP_URL;

public class SignUpPageByFactory {

    @FindBy(css = "#first-name")
    private WebElement firstNameInput;
    @FindBy(css = "input#last-name")
    private WebElement lastNameInput;
    @FindBy(css = "#join-email")
    private WebElement joinEmail;
    @FindBy(css = "button.btn")
    private WebElement agreeAndJoinButton;
    @FindBy(css = "#join-password")
    private WebElement joinPassword;
    @FindBy(css = ".uno-alert strong")
    private WebElement errorAlertMessage;

    private WebDriver driver;
    private ElementHelper elementHelper = new ElementHelper();

    public SignUpPageByFactory(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SignUpPageByFactory open(){
        driver.get(SIGN_UP_URL);
        return this;
    }

    public SignUpPageByFactory enterFirstName(String firstName){
        elementHelper.enterText(firstNameInput, firstName);
        return this;

    }

    public SignUpPageByFactory enterLastName(String lastName){
        elementHelper.enterText(lastNameInput, lastName);
        return this;

    }

    public SignUpPageByFactory enterEmail(){
        elementHelper.enterText(joinEmail, emailGeneration());
        return this;

    }

    public SignUpPageByFactory enterPassword(String password){
        elementHelper.enterText(joinPassword, password);
        return this;

    }

    public SignUpPageByFactory clickJoinButton(){
        agreeAndJoinButton.click();
        return this;

    }

    public SignUpPageByFactory assertErrorMessage(String expectedErrorMessage){

        String actualErrorMessage = errorAlertMessage.getText();

        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Alert message doesn't match");
        return this;

    }

}
