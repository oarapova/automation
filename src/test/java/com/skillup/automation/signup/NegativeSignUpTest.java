package com.skillup.automation.signup;

import com.skillup.automation.ElementHelper;
import com.skillup.automation.LocatorsClass;
import com.skillup.automation.TestRunner;
import com.skillup.automation.pages.SignUpPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class NegativeSignUpTest extends TestRunner {
    private static final String URL = "https://www.linkedin.com/start/join?trk=guest_homepage-basic_nav-header-join";

    private static final String EXPECTED_EMAIL_ERROR_MESSAGE = "Please enter your email address";
    private static final String EXPECTED_PASSWORD_ERROR_MESSAGE = "Please enter your password";

    private ElementHelper helper = new ElementHelper();
    private SignUpPage signUpPage;

    @BeforeMethod
    private void before(){
        signUpPage = new SignUpPage(driver);

    }


    @Test
    public void verifyNotSignUpWithInvalidCredentials(){

        signUpPage.open();
        signUpPage.enterFirstName("firstName");
        signUpPage.enterLastName("lastName");
        //signUpPage.enterEmail();
        signUpPage.enterPassword("1234");
        signUpPage.clickJoinButton();
        signUpPage.assertErrorMessage(EXPECTED_EMAIL_ERROR_MESSAGE);
//

//        WebElement firstNameInput = driver.findElement(By.cssSelector(LocatorsClass.FIRST_NAME_CSS_LOCATOR));
//        WebElement lastNameInput = driver.findElement(By.cssSelector(LocatorsClass.LAST_NAME_CSS_LOCATOR));
//        WebElement emailInput = driver.findElement(By.cssSelector(LocatorsClass.EMAIL_JOIN_CSS_LOCATOR));
//        WebElement passwordInput = driver.findElement(By.cssSelector(LocatorsClass.JOIN_PASS_CSS_LOCATOR));
//
//
//        helper.enterText(firstNameInput,"firstName");
//        helper.enterText(lastNameInput,"lastName");
//        helper.enterText(emailInput,"");
//        helper.enterText(passwordInput,"1234");

//        firstNameInput.clear();
//        firstNameInput.sendKeys("firstName");
//
//        lastNameInput.clear();
//        lastNameInput.sendKeys("lastName");
//
//        emailInput.clear();
//        emailInput.sendKeys("");
//
//        passwordInput.clear();
//        passwordInput.sendKeys("1234");
//
//        WebElement joinButton = driver.findElement(By.cssSelector(LocatorsClass.AGREE_AND_JOIN_CSS_LOCATOR));
//        joinButton.click();


//        WebElement alertMessage = driver.findElement(By.cssSelector(LocatorsClass.ERROR_ALERT_MESSAGE));
//        String alertMessageText = alertMessage.getText();
//
//        Assert.assertEquals(alertMessageText,EXPECTED_EMAIL_ERROR_MESSAGE, "Alert message is incorrect");

    }

    @Test
    public void verifyErrorMessageForEmptyPassword(){
        signUpPage.open();
        signUpPage.enterFirstName("firstName");
        signUpPage.enterLastName("lastName");
        signUpPage.enterEmail();
        signUpPage.enterPassword("");
        signUpPage.clickJoinButton();
        signUpPage.assertErrorMessage(EXPECTED_PASSWORD_ERROR_MESSAGE);

//        WebElement alertMessage = driver.findElement(By.cssSelector(LocatorsClass.ERROR_ALERT_MESSAGE));
//        String alertMessageText = alertMessage.getText();
//
//        Assert.assertEquals(alertMessageText,EXPECTED_PASSWORD_ERROR_MESSAGE, "Alert message is incorrect");
    }


}
