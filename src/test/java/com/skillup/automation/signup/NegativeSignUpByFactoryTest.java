package com.skillup.automation.signup;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pagesByFactory.SignUpPageByFactory;
import org.testng.annotations.Test;


public class NegativeSignUpByFactoryTest extends TestRunner {
    private static final String EXPECTED_PASSWORD_ERROR_MESSAGE = "Please enter your password";
    private static final String EXPECTED_EMAIL_ERROR_MESSAGE = "Please enter your email address";

    @Test
    public void testPassword(){
        SignUpPageByFactory signUpPageByFactory = new SignUpPageByFactory(driver);

        signUpPageByFactory
                .open()
                .enterFirstName("firstName")
                .enterLastName("lastName")
                .enterEmail()
                .enterPassword("")
                .clickJoinButton()
                .assertErrorMessage(EXPECTED_PASSWORD_ERROR_MESSAGE);


    }

    @Test
    public void testEmail(){
        SignUpPageByFactory signUpPageByFactory = new SignUpPageByFactory(driver);

        signUpPageByFactory
                .open()
                .enterFirstName("firstName")
                .enterLastName("lastName")
               // .enterEmail()
                .enterPassword("1234")
                .clickJoinButton()
                .assertErrorMessage(EXPECTED_EMAIL_ERROR_MESSAGE);


    }
}
