package com.skillup.automation.signup;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pages.OnboardingPage;
import com.skillup.automation.pages.SignUpPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PositiveSignUpTest extends TestRunner {

    @Test
    public void positive(){

        signUpPage
                .open()
                .enterFirstName("firstName")
                .enterLastName("lastName")
                .enterEmail()
                .enterPassword("Qw123#5f")
                .clickJoinButton();


        onboardingPage.assertIsOnPage("/onboarding/");
    }

}
