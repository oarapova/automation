package com.skillup.automation;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class MyTest extends TestRunner {
    private static final String EXPECTED_TITLE = "Google";
    private static final String EXPECTED_URL = "google.com";
    private static final String URL = "https://www.google.com";

    @Test
    public void myTest() {
        driver.get(URL);
        String actualTitle = driver.getTitle();
        String asd = "test";

        Assert.assertEquals(actualTitle, EXPECTED_TITLE, "SITE TITLE IS WRONG");
    }

    @Test
    public void myTest2() {
        driver.get(URL);
        String actualUrl = driver.getCurrentUrl();

        String message = String.format("SITE URL IS WRONG actual url: %s, expected part: %s",
                actualUrl,
                EXPECTED_URL);

        Assert.assertTrue(actualUrl.contains(EXPECTED_URL), message);
    }

//    public static void main(String[] args) {
//        System.out.println(String.format(STRING_PATTERN, "1234", "2", "3"));
//    }


}
