package com.skillup.automation.login;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pages.LoginPage;
import com.skillup.automation.pages.WallPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PositiveLoginTest extends TestRunner {

    private LoginPage loginPage;
    private WallPage wallpage;

    @BeforeMethod
    public void before(){
        loginPage = new LoginPage(driver);
         wallpage = new WallPage(driver);

    }

    @Test
    public void positive(){

        loginPage
                .open()
                .setEmailPhone("skillauto904@gmail.com")
                .setPassword("skillauto904")
                .clickSignInButton();

        wallpage.assertIsOnPage("/feed");
    }


}
