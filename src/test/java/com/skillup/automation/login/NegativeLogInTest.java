package com.skillup.automation.login;

import com.skillup.automation.LocatorsClass;
import com.skillup.automation.TestRunner;
import com.skillup.automation.pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NegativeLogInTest extends TestRunner {

    private static final String LOGIN_URL = "https://www.linkedin.com/uas/login";
    private static final String EXPECTED_EMAIL_PHONE_ERROR_MESSAGE = "Please enter an email address or phone number";
    private static final String EXPECTED_PASSWORD_ERROR_MESSAGE = "Please enter a password.";

    private LoginPage loginPage;

    @BeforeMethod
    private void before() {
        loginPage = new LoginPage(driver);

    }


    @Test
    public void verifyMessageForEmptyEmail() {
        //driver.get(LOGIN_URL);

//        loginPage.open();
//        loginPage.setEmailPhone("");
//        loginPage.setPassword("1234");
//        loginPage.clickSignInButton();

        loginPage
                .open()
                .setEmailPhone("")
                .setPassword("1234")
                .clickSignInButton()
                .assertEmailErrorMessage(EXPECTED_EMAIL_PHONE_ERROR_MESSAGE);
//        loginPage.assertEmailErrorMessage(EXPECTED_EMAIL_PHONE_ERROR_MESSAGE);

    }

    @Test
    public void verifyMessageForEmptyPassword() {
        loginPage.open();
        loginPage.setEmailPhone("email@email.com");
        loginPage.setPassword("");
        loginPage.clickSignInButton();
        loginPage.assertPasswordErrorMessage(EXPECTED_PASSWORD_ERROR_MESSAGE);

    }

}
