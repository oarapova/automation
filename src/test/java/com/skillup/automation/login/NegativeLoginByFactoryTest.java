package com.skillup.automation.login;

import com.skillup.automation.TestRunner;
import com.skillup.automation.pagesByFactory.LoginPageByFactory;
import org.testng.annotations.Test;

public class NegativeLoginByFactoryTest extends TestRunner {

    private static final String EXPECTED_EMAIL_PHONE_ERROR_MESSAGE = "Please enter an email address or phone number";

    @Test
    public void test(){

        LoginPageByFactory loginPageByFactory = new LoginPageByFactory(driver);

        loginPageByFactory
                .open()
                .setEmailPhone("")
                .setPassword("1234")
                .clickSignInButton()
                .assertEmailErrorMessage(EXPECTED_EMAIL_PHONE_ERROR_MESSAGE);
    }

}
