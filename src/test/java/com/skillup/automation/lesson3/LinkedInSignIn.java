package com.skillup.automation.lesson3;

import com.skillup.automation.LocatorsClass;
import com.skillup.automation.TestRunner;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LinkedInSignIn extends TestRunner {
    private static final String URL = "https://www.linkedin.com/start/join?trk=guest_homepage-basic_nav-header-join";

    @Test
    public void linkedInSignInXpath() {
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(100000, TimeUnit.MILLISECONDS);

        driver.findElement(By.xpath(LocatorsClass.EMAIL_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.JOIN_PASS_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.AGREE_AND_JOIN_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.FB_BUTTON_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.SIGN_IN_LINK_XPATH_LOCATOR)).isDisplayed();
        driver.quit();
    }

    @Test
    public void linkedInSignInCss() {
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(100000, TimeUnit.MILLISECONDS);

        driver.findElement(By.cssSelector(LocatorsClass.EMAIL_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.JOIN_PASS_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.AGREE_AND_JOIN_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.FB_BUTTON_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.SIGN_IN_LINK_CSS_LOCATOR)).isDisplayed();
        driver.quit();
    }
}
