package com.skillup.automation.lesson3;

public class Cycles {

    public static void main(String[] args) {

        //1. while with precondition
        int index = 0;
        while (index < 10) {
//            while (true)
            //code inside while which will be executed always
            index += 1; // index = index + 1; index++;
            System.out.println(index);

//            if (index == 10){
//                break;
//            }
        }

        for (int i = 0; i < 10; i++){
            System.out.println("From fro:" + i);
        }


    }
}
