package com.skillup.automation.lesson3;

import java.util.*;

public class DataStructure {

    public static void main(String[] args) {
        int[] array = {1, 2, 3};
        int[] array2 = new int[3];

        System.out.println(array[1]);

        for (int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }

        for (int asInt : array){
            System.out.println(asInt);
        }

        System.arraycopy(array, 0, array2, 0, 3);

        System.out.println("_____________________________");

        List<String> stringList = new ArrayList<String>();
        List<String> linkedList = new LinkedList<String>();
        Set<String> set1 = new HashSet<String>();


        stringList.add("1");
        stringList.add("2");
        stringList.add("3");
        stringList.add("4");

        stringList.add(0, "0");

        for (int i =0; i < stringList.size(); i++){
            System.out.println(stringList.get(i));
        }

        doSMth(stringList);
        doSMth(linkedList);
    }


    public static void doSMth(List<String> list){

    }
}
