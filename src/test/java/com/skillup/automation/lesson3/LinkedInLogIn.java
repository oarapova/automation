package com.skillup.automation.lesson3;

import com.skillup.automation.LocatorsClass;
import com.skillup.automation.TestRunner;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LinkedInLogIn extends TestRunner {
    private static final String URL = "https://www.linkedin.com/uas/login";

    @Test
    public void linkedInLogInXpath() {
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(100000, TimeUnit.MILLISECONDS);

        driver.findElement(By.xpath(LocatorsClass.EMAIL_PHONE_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.SESSION_PASS_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.PASS_VISIBILITY_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.SIGN_IN_BUTTON_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.FORGOT_PASS_XPATH_LOCATOR)).isDisplayed();
        driver.findElement(By.xpath(LocatorsClass.JOIN_NOW_LINK_XPATH_LOCATOR)).isDisplayed();
        driver.quit();
    }

    @Test
    public void linkedInLogInCss() {
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(100000, TimeUnit.MILLISECONDS);

        driver.findElement(By.cssSelector(LocatorsClass.EMAIL_PHONE_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.SESSION_PASS_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.PASS_VISIBILITY_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.SIGN_IN_BUTTON_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.FORGOT_PASS_CSS_LOCATOR)).isDisplayed();
        driver.findElement(By.cssSelector(LocatorsClass.JOIN_NOW_LINK_CSS_LOCATOR)).isDisplayed();
        driver.quit();
    }
}
